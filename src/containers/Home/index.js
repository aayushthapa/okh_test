import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

import data from '../../helper/screenData.json';
import { Detail } from '../../navigation'

export function HomeScreen({ navigation }) {
  return (
    <>
      {data.map(screenData => {
        return (
          <TouchableOpacity key={screenData.id} style={styles.buttonStyle} onPress={() => navigation.navigate(Detail, { payload: screenData })}>
            <Text style={styles.buttonTextStyle}>{screenData.title}</Text>
          </TouchableOpacity>
        )
      })}
    </>
  )
}

const styles = StyleSheet.create({
  buttonStyle: {
    padding: 20,
    margin: 30,
    backgroundColor: '#43BBFF',
    alignItems: 'center'
  },
  buttonTextStyle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600'
  }
})