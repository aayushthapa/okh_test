import React from 'react';
import { View, Text, TouchableWithoutFeedback, Dimensions } from 'react-native';

export function DetailScreen({ navigation, route }) {

  const screenData = route.params.payload;

  console.log('screenData', screenData);
  return (
    <TouchableWithoutFeedback onPress={_ => navigation.goBack()}>
      <View style={{ flex: 1, }}>
        <ShowScreenInfo label="short desc" data={screenData.srt_desc} />
        <ShowScreenInfo label="long desc" data={screenData.desc} />
      </View>
    </TouchableWithoutFeedback >

  )
}

function ShowScreenInfo({ label, data }) {
  return (
    <>
      <View style={{ flexDirection: 'row', margin: 20 }}>
        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: 14, fontWeight: '600' }}>{label}:</Text>
        </View>
        <View style={{ flex: 3 }}>
          <Text>{data}</Text>
        </View>
      </View>
    </>
  )
}