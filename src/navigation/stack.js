import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen, DetailScreen } from '../containers';

import * as routesType from './routesType';

const Stack = createStackNavigator();

export function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name={routesType.Home} component={HomeScreen} />
        <Stack.Screen
          name={routesType.Detail}
          component={DetailScreen}
          options={({ route }) => ({
            title: route.params.payload.title
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}